// Copyright Epic Games, Inc. All Rights Reserved.


#include "SnakeGameGameModeBase.h"
#include "Kismet/GameplayStatics.h"
#include "SnakeBase.h"

void ASnakeGameGameModeBase::Speed(ASnakeBase*Snake)
{
	if (Snake) {
		int32 bonus = FMath::RandRange(1, 2);
		if (bonus == 1) {

			Snake->SetActorTickInterval(0.1);
		}
		else if (bonus == 2) {
			Snake->SetActorTickInterval(0.7);
		}
	}
}

void ASnakeGameGameModeBase::Openlevel(ASnakeBase* Snake)
{
	if (Snake) {
		if (Snake->EatOL == 3) UGameplayStatics::OpenLevel(GetWorld(), TEXT("/Game/Main1"));
	}
}
