// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "BlockActor.generated.h"

UCLASS()
class SNAKEGAME_API ABlockActor : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABlockActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	


	
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ABlockActor>BlockClass;

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		class UStaticMeshComponent* MeshBlock;

	/*UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
		void SpawnActors();
	virtual void SpawnActors_Implementation() override;*/

	UFUNCTION()
		void OnOverlap(AActor* OverlappedActor, AActor* OtherActor);

	virtual void Interact(AActor* Interactor, bool bIsHead) override;

};
