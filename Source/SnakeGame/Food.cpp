// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "SnakeElementBase.h"
#include "Kismet/GameplayStatics.h"
#include "SnakeGameGameModeBase.h"


// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	MeshFood = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshFood->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshFood->SetCollisionResponseToAllChannels(ECR_Overlap);
	OnActorBeginOverlap.AddDynamic(this, &AFood::OnOverlap);
}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame


void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead) {
		auto Snake = Cast<ASnakeBase>(Interactor);
		
		
		if (IsValid(Snake)) {
			Snake->AddSnakeElement();
			this->Destroy();
			SpawnActors();
			
			Snake->EatOL++;
			
			ASnakeGameGameModeBase* game_mode = Cast<ASnakeGameGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));//get gameMode
				if (game_mode)
				{
					game_mode->Speed(Snake);
					game_mode->Openlevel(Snake);
				}

			
				
				
				

			
		}
	}
}

void AFood::SpawnActors()
{
	FVector Loc(FMath::RandRange(-450, 450), FMath::RandRange(-450, 450), 50);
	FTransform Trans(Loc);
		AFood* FD = GetWorld()->SpawnActor<AFood>(FoodClass, Trans);
		
	
}

void AFood::OnOverlap(AActor* OverlappedActor, AActor* OtherActor)
{
	if (Cast<ASnakeElementBase>(OtherActor) != nullptr)
	{
		Destroy();
		SpawnActors();

	}
	else {
		Destroy();
		SpawnActors();
	}
}




