// Fill out your copyright notice in the Description page of Project Settings.


#include "BlockActor.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"




// Sets default values
ABlockActor::ABlockActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	MeshBlock = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshBlock->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshBlock->SetCollisionResponseToAllChannels(ECR_Overlap);
	OnActorBeginOverlap.AddDynamic(this, &ABlockActor::OnOverlap);

}

// Called when the game starts or when spawned
void ABlockActor::BeginPlay()
{
	Super::BeginPlay();
	
}

/*void ABlockActor::SpawnActors_Implementation()
{
	TArray<FVector> OutActors;

	for (int i = 0; i < 2; i++) {
		FVector Loc(FMath::RandRange(-450, 450), FMath::RandRange(-450, 450), 50);
		//FTransform Trans(Loc);
		
		OutActors.Add(Loc);	
	}
	FVector FTR = OutActors[1];
	FVector FT = OutActors[0];
	
	if ((FTR.X < (FT.X + 500)) && (FTR.Y < (FT.Y + 500))) {

			FTransform Trans(FTR);
			FTransform Tras(FT);
			ABlockActor* FD = GetWorld()->SpawnActor<ABlockActor>(BlockClass, Trans);
			ABlockActor* FS = GetWorld()->SpawnActor<ABlockActor>(BlockClass, Tras);

		}
		else SpawnActors_Implementation();
	
	

}*/

void ABlockActor::OnOverlap(AActor* OverlappedActor, AActor* OtherActor)
{

	if (ASnakeBase* Snake = Cast<ASnakeBase>(OtherActor)) {
	
		Interact(OtherActor, true);
	
	
	}
		

}

	void ABlockActor::Interact(AActor* Interactor, bool bIsHead)
	{
		if (bIsHead) {
			auto Snake = Cast<ASnakeBase>(Interactor);


			if (IsValid(Snake)) {
				for (ASnakeElementBase* SN : Snake->SnakeElements) SN->Destroy();
				Snake->Destroy();
			}
		}
	}

	
		


// Called every frame

